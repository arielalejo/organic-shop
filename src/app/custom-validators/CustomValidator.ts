import { AbstractControl, ValidationErrors, Validators } from "@angular/forms";

export class CustomValidator{
    static required(control: AbstractControl): ValidationErrors | null {
        if (control.touched){
            return Validators.required;
        }
        return null;
    }

    static urlPatternMatch(control: AbstractControl): ValidationErrors | null{
        const expresion = '(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?';
        const regEx = new RegExp(expresion);

        if(!regEx.test(<string>control.value)){
            return {pattern: true}
        }

        return null;
    }

}