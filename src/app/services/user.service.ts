import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireObject } from '@angular/fire/database';
import * as firebase from 'firebase';
import { AppUser } from '../models/app-user';
import { Observable } from 'rxjs';



const dbNode = "/users/";

@Injectable({
    providedIn: 'root',
})
export class UserService {
    constructor(private firebaseDb: AngularFireDatabase) {}

	//the method saves the user once, and in future calls it updates the user object in the DB
	save(user: firebase.default.User) { // user is the object that is received from firebase when an user logs in.
		this.firebaseDb.object(dbNode + user.uid)
			.update({
				name: user.displayName,
				email: user.email
			})			
	}

	get(uid: string): Observable<AppUser>{		
		return this.firebaseDb.object(dbNode + uid).valueChanges() as Observable<AppUser>;
	}
}
