import { Injectable } from '@angular/core';
import {
    ActivatedRouteSnapshot,
    CanActivate,
	Router,
    RouterStateSnapshot,
} from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { UserService } from './user.service';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root',
})
export class AdminGuard implements CanActivate {
    constructor(
        private authService: AuthService,
		private router: Router
    ) {}

    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean> {

        return this.authService.AppUser$
            .pipe(
                map((userApp) => {
					if (userApp.isAdmin) return userApp.isAdmin
					
					this.router.navigateByUrl("/not-access");
					return false;
				})
            );
    }
}
