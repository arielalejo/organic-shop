import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from '@angular/fire/database';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { IProduct } from '../models/product'

const url = "/products/";

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private db: AngularFireDatabase) { }

  create(product){
    return this.db.list(url).push(product);
  }

  getAllProducts(): Observable<IProduct[]> {
    const productsList: AngularFireList<any> = this.db.list(url);
    return productsList.snapshotChanges().pipe(
      map(changes => changes.map(c => ({key:c.payload.key, ...c.payload.val()})))
    )
  }

  getProductById(id: string): Observable<IProduct>{
    const product : AngularFireObject<any>  = this.db.object(url + id);
    return product.snapshotChanges().pipe(
      map(change  => ({key:change.payload.key , ...change.payload.val()}))
    )    
  }

  update(id: string, product:IProduct){
    return this.db.object(url + id).update(product);
  }

  delete(id:string){
    return this.db.object(url + id).remove();
  }
}
