import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { ActivatedRoute, Router } from '@angular/router';
import * as firebase from 'firebase';
import { Observable, of } from 'rxjs';
import { AppUser } from '../models/app-user';
import { switchMap } from 'rxjs/operators';
import { UserService } from './user.service';

@Injectable({
    providedIn: 'root',
})
export class AuthService {
	loggedUser$ : Observable<firebase.default.User>;
	
    constructor(
		private firebaseAuth: AngularFireAuth, 
		private activeRoute: ActivatedRoute,
		private router: Router,
		private userService: UserService) {

		this.loggedUser$ = this.firebaseAuth.authState;
	}                

    login() {
        const provider  = new firebase.default.auth.GoogleAuthProvider();
		this.firebaseAuth.signInWithPopup(provider).then(
			res => {
				const returnUrl = this.activeRoute.snapshot.queryParamMap.get('previousUrl');
                this.router.navigate([returnUrl || '/']);
			}
		);

	}
  
    logout() {
		this.firebaseAuth.signOut()
		.then(res => this.router.navigate(['/']));
	}

	get AppUser$() : Observable<AppUser>{
		return this.loggedUser$.pipe(
			switchMap((loggedUser) => {
				if (loggedUser) return this.userService.get(loggedUser.uid)

				return of(null);
			})
		)
	}
}
