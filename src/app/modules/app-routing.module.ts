import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminOrdersComponent } from '../components/admin/admin-orders/admin-orders.component';
import { AdminProductsComponent } from '../components/admin/admin-products/admin-products.component';
import { CartComponent } from '../components/cart/cart.component';
import { CheckoutComponent } from '../components/checkout/checkout.component';
import { HomeComponent } from '../components/home/home.component';
import { LoginComponent } from '../components/login/login.component';
import { MyOrdersComponent } from '../components/my-orders/my-orders.component';
import { OrderSuccessComponent } from '../components/order-success/order-success.component';
import { ProductListComponent } from '../components/product-list/product-list.component';
import { AuthGuard } from '../services/auth-guard.service';
import { AdminGuard } from '../services/admin-guard.service';
import { NotAccessComponent } from '../components/not-access/not-access.component';
import { ProductFormComponent } from '../components/admin/products-form/products-form.component';

const routes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'products', component: ProductListComponent },
    { path: 'cart', component: CartComponent },
    { path: 'login', component: LoginComponent },
    { path: 'not-access', component: NotAccessComponent },

    {
        path: 'checkout',
        component: CheckoutComponent,
        canActivate: [AuthGuard],
    },
    {
        path: 'order-sucess',
        component: OrderSuccessComponent,
        canActivate: [AuthGuard],
    },
    {
        path: 'my-orders',
        component: MyOrdersComponent,
        canActivate: [AuthGuard],
    },

    {
        path: 'admin/products/:id',
        component: ProductFormComponent,
        canActivate: [AuthGuard, AdminGuard],
    },
	{
        path: 'admin/products',
        component: AdminProductsComponent,
        canActivate: [AuthGuard, AdminGuard],
    },
    {
        path: 'admin/orders',
        component: AdminOrdersComponent,
        canActivate: [AuthGuard, AdminGuard],
    },

    { path: '**', component: HomeComponent },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule {}
