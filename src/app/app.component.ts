import { Component } from '@angular/core';
import { AuthService } from './services/auth.service';
import { UserService } from './services/user.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
})
export class AppComponent {

    constructor(private authService: AuthService, private userService: UserService){
        this.authService.loggedUser$.subscribe(user => {
            console.log("user: ", user)

            if (user) {
                this.userService.save(user);
            }
        });
    }
}
