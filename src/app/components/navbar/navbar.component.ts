import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { AppUser } from '../../models/app-user';

@Component({
    selector: 'navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
	currentUser: AppUser;

    constructor(private authService: AuthService) {        
        this.authService.AppUser$.subscribe((user:AppUser) => {
            this.currentUser = user;
        })
    }

    ngOnInit(): void {}

    logOut() {
        this.authService.logout();
    }
}
