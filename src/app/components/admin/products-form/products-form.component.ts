import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../../../services/category.service';
import { Observable } from 'rxjs';
import { ProductService } from 'src/app/services/product.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CustomValidator } from 'src/app/custom-validators/CustomValidator';
import { ActivatedRoute, Router } from '@angular/router';
import { take } from 'rxjs/operators';


@Component({
    selector: 'app-products-form',
    templateUrl: './products-form.component.html',
    styleUrls: ['./products-form.component.scss'],
})
export class ProductFormComponent implements OnInit {

    categories$: Observable<any>;
    id: string;
    form: FormGroup = new FormGroup({
      title:  new FormControl('', Validators.required),
      price:  new FormControl('', [Validators.required, Validators.min(0)]),
      category:  new FormControl('', Validators.required),
      imageUrl:  new FormControl('', [Validators.required, CustomValidator.urlPatternMatch]),
    })

    constructor(
        private categoryService: CategoryService,
        private productServie: ProductService,
        private route: ActivatedRoute,
        private router: Router
    ) {
    }

    ngOnInit(): void {
        this.categories$ = this.categoryService.getCategories();
        this.populateProductFields();
    }

    populateProductFields(){
      this.id = this.route.snapshot.paramMap.get("id");
      if (this.id == "new") return;

      this.productServie.getProductById(this.id)
        .pipe(
          take(1)
        ).subscribe(product => {          
          this.title.setValue(product.title);
          this.price.setValue(product.price);
          this.category.setValue(product.category);
          this.imageUrl.setValue(product.imageUrl);
        })
    }

    save() {            
      if (this.id === "new") this.productServie.create(this.form.value);
      else this.productServie.update(this.id, this.form.value);

      this.form.reset();
      this.router.navigate(['/admin/products']);
    }

    delete() {
      if(!confirm("Are you sure to delelete this product?")) return;
      
      this.productServie.delete(this.id)
      this.form.reset();
      this.router.navigate(['/admin/products']);
    }

    get title(){
      return this.form.get("title");
    }

    get price(){
      return this.form.get("price");
    }

    get category(){
      return this.form.get("category");
    }

    get imageUrl(){
      return this.form.get("imageUrl")
    }
}

