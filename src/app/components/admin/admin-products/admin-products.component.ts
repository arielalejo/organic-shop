import { Component, OnDestroy, OnInit, QueryList, ViewChild } from '@angular/core';
import { AngularFireList } from '@angular/fire/database';
import { Observable, Subscription } from 'rxjs';
import { MatSort, Sort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { IProduct } from 'src/app/models/product';
import { ProductService } from 'src/app/services/product.service';


@Component({
  selector: 'app-admin-products',
  templateUrl: './admin-products.component.html',
  styleUrls: ['./admin-products.component.scss']
})
export class AdminProductsComponent implements OnInit, OnDestroy {
  displayedColumns: string[] = ["title", "price", "edit"];
  Suscription: Subscription;
  dataSource: MatTableDataSource<IProduct> = new MatTableDataSource();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private productService: ProductService) {}
  
  ngOnInit(): void {    
    this.Suscription = this.productService.getAllProducts()
      .subscribe( products => {
        this.dataSource.data = products;
      } );
  }

  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    
  }

  handleSearchInput(query: string){    
    query = query.trim();
    query = query.toLowerCase();
    this.dataSource.filter = query;     
  }

  ngOnDestroy():void{
    this.Suscription.unsubscribe()
  }
  
}
