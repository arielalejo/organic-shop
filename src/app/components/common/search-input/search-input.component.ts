import { Component, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'search-input',
  templateUrl: './search-input.component.html',
  styleUrls: ['./search-input.component.scss']
})
export class SearchInputComponent {
  @Output("input-keyup") keyup = new EventEmitter<string>();

  constructor() { }

  search(text:string){
    this.keyup.emit(text);
  }

}
