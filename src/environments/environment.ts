// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAAG-ZgSrIpZT2PWxR3NR7z5mBR-q30WjU",
    authDomain: "org-shop-da9ce.firebaseapp.com",
    projectId: "org-shop-da9ce",
    storageBucket: "org-shop-da9ce.appspot.com",
    messagingSenderId: "656621403548",
    appId: "1:656621403548:web:dc212934169728b3a65c59",
    measurementId: "G-F1778JFZKB"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
